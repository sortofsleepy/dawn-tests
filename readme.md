
Mirai 
=====
An experiment in modern multi-platform code sketch toolkit utilizing Google's Dawn project. Dawn is of course still under development 
but just wanted to get a head start in learning about the new api. 

Dependencies 
=====
* [Boost](https://www.boost.org/) - just for the filesystem library, you don't need to build the whole thing
* [Dawn] (https://dawn.googlesource.com/dawn)


Building Dawn 
=====
First thing to note is that Google actually does not recommend building libraries for integration using depot_tools. Their goal from what I understand is to eventually 
provide CMake support. However the following should work.

* Download and setup [depot_tools](https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up)
* Clone the Dawn project

Some extra things to set up 
=====
* For some reason, the Python setup that ships with depot_tools does not include the [pywin32](https://github.com/mhammond/pywin32) package which is needed to build on Windows. The easist way I've found to install the package is to cd into `depot_tools\bootstrap-3_8_0_chromium_8_bin\python\bin` and run `python -m pip install pywin32` which should utilize the depot_tools python and properly install the pywin32 package into depot_tools.
* You will need to setup the [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)


Final Steps
======
Now, you can continue following the directions found [here](https://dawn.googlesource.com/dawn/+/HEAD/docs/buiding.md). 
The settings you ought to use are 

* `is_clang`=false
* `dawn_complete_static_libs` = true
* `dawn_enable_d3d12`=false (see gotchas on why this might be necessary)
* `is_debug=false` (I was not able to get a Debug build working :/) 

What files to include in your project
======
For headers you will need to include everything in 
* `<repo root>/src` 
* `<repo root>/<build folder>/gen/src`

For libraries 

* glfw.lib
* dawn_bindings.lib
* dawn_sample_utils.lib
* dawn_utils.lib
* libdawn_native.lib
* libdawn_proc.lib
* libdawn_wire.lib
* libshaderc.lib / shaderc_combined.lib(if you build your own)

Gotchas 
========
* `webgpu_cpp.cpp` needs to be included in your project
* The `shaderc` third party library gets compiled with missing symbols somehow, I have not found the correct magical incantation to correctly compile with all the necessary functions. As a result, you may (for now hopefully) have to compile  `shaderc` on your own for now. If you need to compile `shaderc` see the named section below.
* DirectX might cause issues(see following section)
* The samples default to utilizing an object called a `TerribleCommandBuffer`, along with using that object, it also sets up `dawn_wire::WireServer` and `dawn_wire::WireClient` objects; currently it's unclear as to what the objects in the `dawn_wire` namespace are for exactly, but I had to change the SampleUtils file to not utilize those objects in order to get things to work. 


Disabling DirectX (possibly)
=====
There is a slight issue with how one DirectX library is linked to the final output. 
* If you are building a shared library or using gn to build your project directly, you can add the parameter libs=["dxguid.lib"] to your build arguments(note I have not fully tested this).
* Visual Studio should automatically have the DirectX paths available assuming you set things up using a project template, you can just set up dxguid.lib within the linker section of your project's properties


Building Shaderc (possibly)
=======
There is a possibility you may have to build the `shaderc` library yourself. For some reason, building it during your `dawn` build ends up with a final library that's still missing some symbols. It is currently unclear as to why this is, my assumption is that there is a build setting that needs to get applied. There is a possibly related setting , called `dawn_enable_cross_reflection` that can be set; however I have tried building with and without that build setting being set with the same result.


Building `shaderc` is fairly straightforward

* You will also need to clone SPIRV-Tools into the third_party folder.
* Like the readme suggests, you'll also have to clone glslang into the third_party folder. You however will not have to build, but just clone.
* You can but don't have to install Google Test. If you don't install Google Test(which I believe also should go in the third_party folder), you need to pass -DSHADERC_SKIP_TESTS=true (assuming you're using CMake)
* It's best to build release, otherwise with debug content the resulting libraries are quite large.




