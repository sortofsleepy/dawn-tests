
#include "mirai/baserenderer.h"
#include <algorithm>

namespace mirai
{
	wgpu::TextureView BaseRenderer::CreateDefaultDepthStencilView(const wgpu::Device& device)
	{
		wgpu::TextureDescriptor descriptor;
		descriptor.dimension = wgpu::TextureDimension::e2D;
		descriptor.size.width = format.width;
		descriptor.size.height = format.height;
		descriptor.size.depth = 1;
		descriptor.arrayLayerCount = 1;
		descriptor.sampleCount = 1;
		descriptor.format = wgpu::TextureFormat::Depth24PlusStencil8;
		descriptor.mipLevelCount = 1;
		descriptor.usage = wgpu::TextureUsage::OutputAttachment;
		auto depthStencilTexture = device.CreateTexture(&descriptor);
		return depthStencilTexture.CreateView();
	}


	wgpu::Device BaseRenderer::createDevice(GLFWwindow * window)
	{
	

		instance = std::make_unique<dawn_native::Instance>();
		utils::DiscoverAdapter(instance.get(), window, backendType);

		// Get an adapter for the backend to use, and create the device.
		dawn_native::Adapter backendAdapter;
		{
			std::vector<dawn_native::Adapter> adapters = instance->GetAdapters();
			auto adapterIt = std::find_if(adapters.begin(), adapters.end(),
				[=](const dawn_native::Adapter adapter) -> bool {
				wgpu::AdapterProperties properties;
				adapter.GetProperties(&properties);
				return properties.backendType == backendType;
			});
			ASSERT(adapterIt != adapters.end());
			backendAdapter = *adapterIt;
		}

		WGPUDevice backendDevice = backendAdapter.CreateDevice();
		DawnProcTable backendProcs = dawn_native::GetProcs();

		binding = utils::CreateBinding(backendType, window, backendDevice);
		if (binding == nullptr) {
			return wgpu::Device();
		}

	
		dawnProcSetProcs(&backendProcs);
		backendProcs.deviceSetUncapturedErrorCallback(backendDevice, PrintDeviceError, nullptr);

		// acquire device. 
		device = wgpu::Device::Acquire(backendDevice);

		// create default queue. 
		queue = wgpuDeviceCreateQueue(device.Get());
		
		return device;
	}




}
