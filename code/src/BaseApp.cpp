
#include "mirai/baseapp.h"

using namespace std;

namespace mirai
{
	void BaseApp::setupFoundation(RendererFormat format)
	{

		// initialize GLFW 
		if (!glfwInit())
		{
			throw std::runtime_error("Unable to initialize GLFW for some reason");
		}

		utils::SetupGLFWWindowHintsForBackend(format.type);

		window = glfwCreateWindow(format.width, format.height, "Dawn window", nullptr, nullptr);
		if (!window) {
			throw std::runtime_error("Unable to initialize GLFW window for some reason");
		}

		windowWidth = format.width;
		windowHeight = format.height;

		// setup the underlying rendering engine
		renderer = new BaseRenderer(format);

		// build the device used to render things
		renderer->createDevice(window);

	
	}

}