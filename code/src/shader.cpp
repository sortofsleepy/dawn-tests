#include "mirai/shader.h"
#include "mirai/log.h"
namespace mirai
{
	Shader::Shader(Format fmt)
	{
		this->format = fmt;

	
	}

	void Shader::generateModules(wgpu::Device *device)
	{
		if(format.hasVertex())
		{
	
		
			vertexStage.nextInChain = nullptr;
			vertexStage.entryPoint = "main";
			vertexStage.module = utils::CreateShaderModule(device->Get(), utils::SingleShaderStage::Vertex, format.getVertex().c_str()).Release();
		
		}
		if(format.hasFragment())
		{
		
			fragmentStage.nextInChain = nullptr;
			fragmentStage.entryPoint = "main";
			fragmentStage.module = utils::CreateShaderModule(device->Get(), utils::SingleShaderStage::Fragment, format.getFragment().c_str()).Release();

		}

		modulesGenerated = true;

	}

	void Shader::releaseModules(bool computeRelease)
	{
		wgpuShaderModuleRelease(vertexStage.module);
		wgpuShaderModuleRelease(fragmentStage.module);

		if(computeRelease)
		{
			wgpuShaderModuleRelease(computeStage.module);
		}
		
		modulesGenerated = false;
	}


}