#include "mirai/mesh.h"


namespace mirai
{

	void Mesh::addColorState(WGPUTextureFormat textureFormat, WGPUColorWriteMask colorWriteMask, WGPUBlendDescriptor alphaBlendDescriptor, WGPUBlendDescriptor colorStateBlendDescriptor)
	{
		WGPUColorStateDescriptor colorStateDescriptor;
	
		colorStateDescriptor.nextInChain = nullptr;
		colorStateDescriptor.format = WGPUTextureFormat_BGRA8Unorm;
		colorStateDescriptor.alphaBlend = alphaBlendDescriptor;
		colorStateDescriptor.colorBlend = colorStateBlendDescriptor;
		colorStateDescriptor.writeMask = colorWriteMask;

		colorStates.push_back(colorStateDescriptor);
	}

	void Mesh::addColorState(WGPUTextureFormat textureFormat)
	{
		WGPUColorWriteMask colorWriteMask = WGPUColorWriteMask_All;
		WGPUBlendDescriptor alphaBlendDescriptor = mirai::utils::defaultBlendDescriptor();
		WGPUBlendDescriptor colorStateBlendDescriptor = mirai::utils::defaultBlendDescriptor();

		
		WGPUColorStateDescriptor colorStateDescriptor;

		colorStateDescriptor.nextInChain = nullptr;
		colorStateDescriptor.format = textureFormat;
		colorStateDescriptor.alphaBlend = alphaBlendDescriptor;
		colorStateDescriptor.colorBlend = colorStateBlendDescriptor;
		colorStateDescriptor.writeMask = colorWriteMask;

		colorStates.push_back(colorStateDescriptor);
	}


	void Mesh::compilePipeline()
	{
		WGPURenderPipelineDescriptor descriptor;
		descriptor.label = nullptr;
		descriptor.nextInChain = nullptr;

		descriptor.vertexStage = shader->getVertexStage();

		// need to do it like this weirdly enough,otherwise pipeline won't build. 
		auto stage = shader->getFragmentStage();
		WGPUProgrammableStageDescriptor fragmentStage;
		fragmentStage.nextInChain = stage.nextInChain;
		fragmentStage.module = stage.module;
		fragmentStage.entryPoint = stage.entryPoint;
		descriptor.fragmentStage = &fragmentStage;


		descriptor.sampleCount = sampleCount;



		
	}

	void Mesh::defaultSettings()
	{

		// ================ SETUP DEFAULT COLOR STATES ================= //
		
		WGPUColorStateDescriptor colorStateDescriptor;
		WGPUTextureFormat colorStateTextureFormat;
		WGPUColorWriteMask colorWriteMask;
		WGPUBlendDescriptor alphaBlendDescriptor;
		WGPUBlendDescriptor colorBlendDescriptor;
		alphaBlendDescriptor.operation = WGPUBlendOperation_Add;
		alphaBlendDescriptor.srcFactor = WGPUBlendFactor_One;
		alphaBlendDescriptor.dstFactor = WGPUBlendFactor_One;

		colorBlendDescriptor.operation = WGPUBlendOperation_Add;
		colorBlendDescriptor.srcFactor = WGPUBlendFactor_One;
		colorBlendDescriptor.dstFactor = WGPUBlendFactor_One;

		colorStateTextureFormat = WGPUTextureFormat_BGRA8Unorm;
		colorWriteMask = WGPUColorWriteMask_All;

		colorStateDescriptor.nextInChain = nullptr;
		colorStateDescriptor.format = WGPUTextureFormat_BGRA8Unorm;
		colorStateDescriptor.alphaBlend = alphaBlendDescriptor;
		colorStateDescriptor.colorBlend = colorBlendDescriptor;
		colorStateDescriptor.writeMask = colorWriteMask;

		colorStates.push_back(colorStateDescriptor);
	}


	void Mesh::defaultPipeline(WGPUDevice device)
	{

		defaultSettings();

		WGPURenderPipelineDescriptor descriptor;
		descriptor.label = nullptr;
		descriptor.nextInChain = nullptr;

		// setup vertex shader
		descriptor.vertexStage = shader->getVertexStage();

		// need to do it like this weirdly enough,otherwise pipeline won't build. 
		auto stage = shader->getFragmentStage();
		WGPUProgrammableStageDescriptor fragmentStage;
		fragmentStage.nextInChain = stage.nextInChain;
		fragmentStage.module = stage.module;
		fragmentStage.entryPoint = stage.entryPoint;
		descriptor.fragmentStage = &fragmentStage;
	

		descriptor.sampleCount = this->sampleCount;

		descriptor.colorStateCount = colorStates.size();
		descriptor.colorStates = colorStates.data();

		WGPUPipelineLayoutDescriptor pl;
		pl.nextInChain = nullptr;
		pl.label = nullptr;
		pl.bindGroupLayoutCount = 0;
		pl.bindGroupLayouts = nullptr;
		descriptor.layout = wgpuDeviceCreatePipelineLayout(device, &pl);

		WGPUVertexStateDescriptor vertexState;
		vertexState.nextInChain = nullptr;
		vertexState.indexFormat = indexFormat;
		vertexState.vertexBufferCount = 0;
		vertexState.vertexBuffers = nullptr;
		descriptor.vertexState = &vertexState;

		
		rasterizationState.nextInChain = nullptr;
		rasterizationState.frontFace = WGPUFrontFace_CCW;
		rasterizationState.cullMode = cullMode;
		rasterizationState.depthBias = 0;
		rasterizationState.depthBiasSlopeScale = 0.0;
		rasterizationState.depthBiasClamp = 0.0;
		descriptor.rasterizationState = &rasterizationState;

		descriptor.primitiveTopology = topology;
		descriptor.sampleMask = 0xFFFFFFFF;
		descriptor.alphaToCoverageEnabled = false;

		descriptor.depthStencilState = nullptr;

		pipeline = wgpuDeviceCreateRenderPipeline(device, &descriptor);


	}

}