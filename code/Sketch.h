#pragma once
#include "mirai/baseapp.h"
#include "mirai/mesh.h"
#include "mirai/shader.h"
#include "mirai/geometry.h"
#include "mirai/file.h"
#include "libraries/dawn/SampleUtils.h"

#include "mirai/shader.h"
#include "mirai/mesh.h"
using namespace mirai;


WGPUDevice device;
WGPUQueue queue;
WGPUSwapChain swapchain;
WGPURenderPipeline pipeline,testPipeline;

WGPUTextureFormat swapChainFormat;


void createPipeline(ShaderRef shader)
{
	WGPURenderPipelineDescriptor descriptor;
	descriptor.label = nullptr;
	descriptor.nextInChain = nullptr;


	descriptor.vertexStage = shader->getVertexStage();
	descriptor.fragmentStage = &shader->getFragmentStage();

	descriptor.sampleCount = 1;

	WGPUBlendDescriptor blendDescriptor;
	blendDescriptor.operation = WGPUBlendOperation_Add;
	blendDescriptor.srcFactor = WGPUBlendFactor_One;
	blendDescriptor.dstFactor = WGPUBlendFactor_One;
	WGPUColorStateDescriptor colorStateDescriptor;
	colorStateDescriptor.nextInChain = nullptr;
	colorStateDescriptor.format = WGPUTextureFormat_BGRA8Unorm;
	colorStateDescriptor.alphaBlend = blendDescriptor;
	colorStateDescriptor.colorBlend = blendDescriptor;
	colorStateDescriptor.writeMask = WGPUColorWriteMask_All;

	descriptor.colorStateCount = 1;
	descriptor.colorStates = &colorStateDescriptor;

	WGPUPipelineLayoutDescriptor pl;
	pl.nextInChain = nullptr;
	pl.label = nullptr;
	pl.bindGroupLayoutCount = 0;
	pl.bindGroupLayouts = nullptr;
	descriptor.layout = wgpuDeviceCreatePipelineLayout(device, &pl);

	WGPUVertexStateDescriptor vertexState;
	vertexState.nextInChain = nullptr;
	vertexState.indexFormat = WGPUIndexFormat_Uint32;
	vertexState.vertexBufferCount = 0;
	vertexState.vertexBuffers = nullptr;
	descriptor.vertexState = &vertexState;

	WGPURasterizationStateDescriptor rasterizationState;
	rasterizationState.nextInChain = nullptr;
	rasterizationState.frontFace = WGPUFrontFace_CCW;
	rasterizationState.cullMode = WGPUCullMode_None;
	rasterizationState.depthBias = 0;
	rasterizationState.depthBiasSlopeScale = 0.0;
	rasterizationState.depthBiasClamp = 0.0;
	descriptor.rasterizationState = &rasterizationState;

	descriptor.primitiveTopology = WGPUPrimitiveTopology_TriangleList;
	descriptor.sampleMask = 0xFFFFFFFF;
	descriptor.alphaToCoverageEnabled = false;

	descriptor.depthStencilState = nullptr;

	pipeline= wgpuDeviceCreateRenderPipeline(device, &descriptor);
	
}

class Sketch : public mirai::BaseApp
{

	ShaderRef shader;
	MeshRef mesh;

public:
	Sketch() = default;

	void setup() override
	{

		shader = Shader::create(Shader::Format()
			.vertex(io::File::loadTextFile("vertex.glsl"))
			.fragment(io::File::loadTextFile("fragment.glsl")));

	
		device = renderer->getDevice().Get();

		shader->generateModules(&renderer->getDevice());
		queue = renderer->getQueue();
		
		// generate a swapchain
		swapchain = renderer->generateSwapchain(device);


		mesh = Mesh::create(device, shader);
	
		//mesh.generatePipeline(device, shader);
		
		
	}



	void draw() override
	{

		WGPUTextureView backbufferView = wgpuSwapChainGetCurrentTextureView(swapchain);
		WGPURenderPassDescriptor renderpassInfo;
		renderpassInfo.nextInChain = nullptr;
		renderpassInfo.label = nullptr;
		WGPURenderPassColorAttachmentDescriptor colorAttachment;
		{
			colorAttachment.attachment = backbufferView;
			colorAttachment.resolveTarget = nullptr;
			colorAttachment.clearColor = { 0.0f, 0.0f, 0.0f, 0.0f };
			colorAttachment.loadOp = WGPULoadOp_Clear;
			colorAttachment.storeOp = WGPUStoreOp_Store;
			renderpassInfo.colorAttachmentCount = 1;
			renderpassInfo.colorAttachments = &colorAttachment;
			renderpassInfo.depthStencilAttachment = nullptr;
		}
		WGPUCommandBuffer commands;
		{
			WGPUCommandEncoder encoder = wgpuDeviceCreateCommandEncoder(device, nullptr);

			WGPURenderPassEncoder pass = wgpuCommandEncoderBeginRenderPass(encoder, &renderpassInfo);
			//wgpuRenderPassEncoderSetPipeline(pass, pipeline);
			wgpuRenderPassEncoderSetPipeline(pass, mesh->getPipeline());

			wgpuRenderPassEncoderDraw(pass, 3, 1, 0, 0);
			wgpuRenderPassEncoderEndPass(pass);
			wgpuRenderPassEncoderRelease(pass);

			commands = wgpuCommandEncoderFinish(encoder, nullptr);
			wgpuCommandEncoderRelease(encoder);
		}

		wgpuQueueSubmit(queue, 1, &commands);
		wgpuCommandBufferRelease(commands);
		wgpuSwapChainPresent(swapchain);
		wgpuTextureViewRelease(backbufferView);
		/*
		 * 
		WGPUTextureView backbufferView = wgpuSwapChainGetCurrentTextureView(swapchain);
		WGPURenderPassDescriptor renderpassInfo;
		renderpassInfo.nextInChain = nullptr;
		renderpassInfo.label = nullptr;
		WGPURenderPassColorAttachmentDescriptor colorAttachment;
		{
			colorAttachment.attachment = backbufferView;
			colorAttachment.resolveTarget = nullptr;
			colorAttachment.clearColor = { 0.0f, 0.0f, 0.0f, 0.0f };
			colorAttachment.loadOp = WGPULoadOp_Clear;
			colorAttachment.storeOp = WGPUStoreOp_Store;
			renderpassInfo.colorAttachmentCount = 1;
			renderpassInfo.colorAttachments = &colorAttachment;
			renderpassInfo.depthStencilAttachment = nullptr;
		}
		WGPUCommandBuffer commands;
		{
			WGPUCommandEncoder encoder = wgpuDeviceCreateCommandEncoder(device, nullptr);

			WGPURenderPassEncoder pass = wgpuCommandEncoderBeginRenderPass(encoder, &renderpassInfo);
			//wgpuRenderPassEncoderSetPipeline(pass, pipeline);
			wgpuRenderPassEncoderSetPipeline(pass, mesh.pipeline);
			
			wgpuRenderPassEncoderDraw(pass, 3, 1, 0, 0);
			wgpuRenderPassEncoderEndPass(pass);
			wgpuRenderPassEncoderRelease(pass);

			commands = wgpuCommandEncoderFinish(encoder, nullptr);
			wgpuCommandEncoderRelease(encoder);
		}

		wgpuQueueSubmit(queue, 1, &commands);
		wgpuCommandBufferRelease(commands);
		wgpuSwapChainPresent(swapchain);
		wgpuTextureViewRelease(backbufferView);

		 */

	}
};



/*
 *
		WGPURenderPipelineDescriptor descriptor;
		descriptor.label = nullptr;
		descriptor.nextInChain = nullptr;


		descriptor.vertexStage = shader->getVertexStage();
		descriptor.fragmentStage = &shader->getFragmentStage();

		descriptor.sampleCount = 1;

		WGPUBlendDescriptor blendDescriptor;
		blendDescriptor.operation = WGPUBlendOperation_Add;
		blendDescriptor.srcFactor = WGPUBlendFactor_One;
		blendDescriptor.dstFactor = WGPUBlendFactor_One;
		WGPUColorStateDescriptor colorStateDescriptor;
		colorStateDescriptor.nextInChain = nullptr;
		colorStateDescriptor.format = WGPUTextureFormat_BGRA8Unorm;
		colorStateDescriptor.alphaBlend = blendDescriptor;
		colorStateDescriptor.colorBlend = blendDescriptor;
		colorStateDescriptor.writeMask = WGPUColorWriteMask_All;

		descriptor.colorStateCount = 1;
		descriptor.colorStates = &colorStateDescriptor;

		WGPUPipelineLayoutDescriptor pl;
		pl.nextInChain = nullptr;
		pl.label = nullptr;
		pl.bindGroupLayoutCount = 0;
		pl.bindGroupLayouts = nullptr;
		descriptor.layout = wgpuDeviceCreatePipelineLayout(device, &pl);

		WGPUVertexStateDescriptor vertexState;
		vertexState.nextInChain = nullptr;
		vertexState.indexFormat = WGPUIndexFormat_Uint32;
		vertexState.vertexBufferCount = 0;
		vertexState.vertexBuffers = nullptr;
		descriptor.vertexState = &vertexState;

		WGPURasterizationStateDescriptor rasterizationState;
		rasterizationState.nextInChain = nullptr;
		rasterizationState.frontFace = WGPUFrontFace_CCW;
		rasterizationState.cullMode = WGPUCullMode_None;
		rasterizationState.depthBias = 0;
		rasterizationState.depthBiasSlopeScale = 0.0;
		rasterizationState.depthBiasClamp = 0.0;
		descriptor.rasterizationState = &rasterizationState;

		descriptor.primitiveTopology = WGPUPrimitiveTopology_TriangleList;
		descriptor.sampleMask = 0xFFFFFFFF;
		descriptor.alphaToCoverageEnabled = false;

		descriptor.depthStencilState = nullptr;

 */