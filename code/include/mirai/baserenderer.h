#pragma once

#include "GLFW/glfw3.h"
#include <memory>
#include <dawn/dawn_proc.h>
#include "utils/BackendBinding.h"
#include "utils/GLFWUtils.h"
#include "mirai/log.h"

namespace mirai
{

	

	//! Describes the base properties of how a renderer should initialize itself.
	//! Includes backend selection + width/height of surface you want to create.
	//! Defaults to Vulkan and a 640x480 surface.
	struct RendererFormat
	{
		wgpu::BackendType type = wgpu::BackendType::Vulkan;
		int width = 640;
		int height = 480;
	};
	
	class BaseRenderer
	{
	public:

		BaseRenderer(RendererFormat format)
		{
			this->format = format;
			backendType = format.type;
		}

		//! Initializes a device object. 
		wgpu::Device createDevice(GLFWwindow * window);

		//! Returns the underlying engine type.
		wgpu::BackendType getEngineType() { return backendType; }


		//! Sets the current clear color.
		void setClearColor(float r, float g, float b, float a = 1.0f);


		// get a default blend descriptor.
		static WGPUBlendDescriptor getDefaultBlend()
		{
			WGPUBlendDescriptor blendDescriptor;
			blendDescriptor.operation = WGPUBlendOperation_Add;
			blendDescriptor.srcFactor = WGPUBlendFactor_One;
			blendDescriptor.dstFactor = WGPUBlendFactor_One;

			return blendDescriptor;
		}

		//! Returns the preferred swapchain texture format. 
		WGPUTextureFormat getPreferredSwapchainTextureFormat()
		{
			return binding->GetPreferredSwapChainTextureFormat();
		}

		uint64_t getSwapChainImplementation() {
			return binding->GetSwapChainImplementation();
		}

		//! Returns the primary device queue
		WGPUQueue getQueue() const { return queue; }


		//! Returns a reference to the device. 
		wgpu::Device getDevice() { return device; }




		WGPUSwapChain generateSwapchain(WGPUDevice _device)
		{
			WGPUSwapChainDescriptor descriptor;
			descriptor.nextInChain = nullptr;
			descriptor.label = nullptr;
			descriptor.implementation = getSwapChainImplementation();
			auto swap = wgpuDeviceCreateSwapChain(_device, nullptr, &descriptor);
			const auto swapChainFormat = static_cast<WGPUTextureFormat>(getPreferredSwapchainTextureFormat());
			wgpuSwapChainConfigure(swap, swapChainFormat, WGPUTextureUsage_OutputAttachment, 640,480);

			return swap;

		}

		
	protected:
		
		wgpu::TextureView CreateDefaultDepthStencilView(const wgpu::Device& device);
		
		//! format object with backend settings settings
		RendererFormat format;

		//! Backend engine to use
		wgpu::BackendType backendType;
		
		//! Device object for the app
		wgpu::Device device;

		//! Device queue. 
		WGPUQueue queue;

		//! Instance object representing device properties
		std::unique_ptr<dawn_native::Instance> instance;

		// a reference to the engine used. 
		utils::BackendBinding* binding;

		
	};
}
