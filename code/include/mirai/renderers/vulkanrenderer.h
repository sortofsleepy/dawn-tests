#pragma once

#include "mirai/baserenderer.h"

namespace mirai
{
	class VulkanRenderer : public BaseRenderer
	{
	public:
		VulkanRenderer()
		{
			format.type = wgpu::BackendType::Vulkan;
		}
	};
}