#pragma once


#include <memory>
#include "geometry.h"
#include "shader.h"
#include "baserenderer.h"
#include "mirai/vbo.h"
#include "mirai/utils.h"


// re-define primitive enums to have shorter names.
#define TRIANGLE_LIST WGPUPrimitiveTopology_TriangleList
#define POINT_LIST WGPUPrimitiveTopology_PointList
#define LINE_LIST WGPUPrimitiveTopology_LineList
#define LINE_STRIP WGPUPrimitiveTopology_LineStrip
#define TRIANGLE_STRIP WGPUPrimitiveTopology_TriangleStrip
#define FORCE32 WGPUPrimitiveTopology_Force32


namespace mirai
{

	

	typedef std::shared_ptr<class Mesh>MeshRef;

	class Mesh
	{
	public:
		Mesh(WGPUDevice device, ShaderRef shader)
		{
			indexFormat = WGPUIndexFormat_Uint32;
			topology = WGPUPrimitiveTopology_TriangleList;
			sampleCount = 1;
			cullMode = WGPUCullMode_None;

			this->shader = shader;
			this->device = device;
			
			defaultPipeline(device);
		}

		static MeshRef create(WGPUDevice device, ShaderRef shader)
		{
			return MeshRef(new Mesh(device, shader));
		}

		//! Adds a new color state to the mesh. 
		void addColorState(
			WGPUTextureFormat textureFormat = WGPUTextureFormat_BGRA8Unorm,
			WGPUColorWriteMask colorWriteMask = WGPUColorWriteMask_All,
			WGPUBlendDescriptor alphaBlendDescriptor = mirai::utils::defaultBlendDescriptor(),
			WGPUBlendDescriptor colorStateBlendDescriptor = mirai::utils::defaultBlendDescriptor());

		void addColorState(WGPUTextureFormat textureFormat);

		//! Returns the pipeline for the mesh. 
		WGPURenderPipeline getPipeline() { return pipeline; }

		//! Compiles the pipeline with current settings. 
		void compilePipeline();
		
	protected:

		//! Set some sensible defaults for settings
		void defaultSettings();
		
		//! generates a default pipeline that's enough to get going. 
		void defaultPipeline(WGPUDevice device);

		//! Shader for the mesh 
		ShaderRef shader;

		//! Render pipeline for the mesh 
		WGPURenderPipeline pipeline;

		//! Reference to the device used to build up the mesh. 
		WGPUDevice device;

		//! cull mode for the mesh 
		WGPUCullMode cullMode;

		//! Sample count for the mesh 
		uint32_t sampleCount;

		//! Topology mode for the mesh 
		WGPUPrimitiveTopology topology;

		//! Holds the rasterization state for the mesh. 
		WGPURasterizationStateDescriptor rasterizationState;

		//! The index format type for the mesh
		WGPUIndexFormat indexFormat;

		//! Holds all the color states for the mesh.
		std::vector<WGPUColorStateDescriptor> colorStates;
		
		
	};
}
