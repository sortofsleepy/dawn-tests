#pragma once

#include "GLFW/glfw3.h"
#include "mirai/baserenderer.h"
#include "mirai/file.h"
namespace mirai
{

	class BaseApp
	{
	public:
		BaseApp()
		{
			setupFoundation(RendererFormat());
		}
		
		BaseApp(RendererFormat format){
			setupFoundation(format);
			
		}

	
		WGPUTextureFormat getPreferredColorFormat()
		{
			return renderer->getPreferredSwapchainTextureFormat();
		}

		virtual void setup() = 0;
		virtual void draw() = 0;

		//! Mainly handles loop. Virtual to allow for a little flexibility. 
		virtual void run()
		{
		
			while(!glfwWindowShouldClose(window))
			{
				draw();
				glfwPollEvents();
			}
			 
		};

		

		//! Return the window Aspect ratio
		float getAspectRatio() { return windowWidth / windowHeight;  }

		wgpu::Device* getDevice() const { return &renderer->getDevice();  }


		int getWindowWidth() { return windowWidth; }
		int getWindowHeight() { return windowHeight; }
		
		//! Loads a text based asset. Assumes asset lies at the default output path of "assets"
		std::string loadTextAsset(std::string path)
		{
			return io::File::loadTextFile(path);
		}
	protected:

		//! Sets up the foundational aspects of the engine. 
		void setupFoundation(RendererFormat format);

		//! a reference to the underlying rendering engine.
		BaseRenderer * renderer;

		//! Window width and height 
		int windowWidth, windowHeight;

		//! Reference to the window itself
		GLFWwindow * window;
	};
}