#pragma once


#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "common/Log.h"
#include <common/Assert.h>

//! Gets the filename of the value returned from __FILE__ without
//! the full path.
// TODO currently outputs log.h as filename :/
static std::string getShortFilename() {
	std::string file = __FILE__;

	std::stringstream ss{ file };

	std::string tok;
	std::vector<std::string> internal;

	while (std::getline(ss, tok, '/')) {
		internal.push_back(tok);
	}

	return internal.back();
}
#define LOG_I( stream ) std::cout <<" - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl
#define LOG_W( stream ) std::cout <<" :WARNING - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl
#define LOG_E( stream ) std::cout <<" :ERROR - " << __FUNCTION__ << " Line: " <<__LINE__  << " [ " << stream << " ]" <<std::endl

#define DAWN_LOG(stream) dawn::LogMessage() << stream;
#define DAWN_ERROR(stream) dawn::ErrorLog() << stream;




static void PrintDeviceError(WGPUErrorType errorType, const char* message, void*) {
	const char* errorTypeName = "";
	switch (errorType) {
	case WGPUErrorType_Validation:
		errorTypeName = "Validation";
		break;
	case WGPUErrorType_OutOfMemory:
		errorTypeName = "Out of memory";
		break;
	case WGPUErrorType_Unknown:
		errorTypeName = "Unknown";
		break;
	case WGPUErrorType_DeviceLost:
		errorTypeName = "Device lost";
		break;
	default:
		UNREACHABLE();
		return;
	}
	dawn::ErrorLog() << errorTypeName << " error: " << message;
}

static void PrintGLFWError(int code, const char* message) {
	dawn::ErrorLog() << "GLFW error: " << code << " - " << message;
}
