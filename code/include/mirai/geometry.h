#pragma once

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include <vector>

namespace mirai
{

	//! Base make up of a geometry component.
	struct Geometry
	{
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> uvs;
		std::vector<uint32_t> indices;

		bool hasUvs() { return uvs.empty(); }
		bool hasIndices() { return indices.empty(); }
	};


	namespace geo
	{

		//! Generates vertices for a triangle. 
		static Geometry Triangle()
		{
			Geometry geo = Geometry();
			geo.vertices = {
				glm::vec3(-1., -1.,0.),
				glm::vec3(-1., 4.,0.),
				glm::vec3(4., -1.,0.)
			};

			return geo;
		}
	}
	
	
}