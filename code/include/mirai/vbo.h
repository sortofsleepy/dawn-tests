#pragma once

#include <memory>
#include <dawn/webgpu_cpp.h>
#include <vector>

namespace mirai
{
	typedef std::shared_ptr<class Vbo>VboRef;

	// a thin wrapper around buffer objects 
	class Vbo
	{
	public:
		Vbo() = default;

		static VboRef create()
		{
			return VboRef(new Vbo);  
		}

		template<typename T>
		void setData(std::vector<T> data);

	protected:
		wgpu::Buffer buffer;
	};
}
