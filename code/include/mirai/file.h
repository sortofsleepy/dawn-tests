#pragma once
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include "boost/filesystem.hpp"

using namespace std;
namespace fs = boost::filesystem;

namespace mirai { namespace io {

	class File
	{
	public:
		File() = default;


		
		//! Loads a text based file like a GLSL shader.
		//! By default, it looks inside of a folder called "output",then looks for a folder called "assets" change if not necessary
		static std::string loadTextFile(std::string path,std::string defaultOutput="output/assets")
		{
			fs::path p(path);

			// ensure we start at the application directory
			p = fs::current_path() / defaultOutput / p;

			// build stream for reading
			fs::ifstream stream;

			// if path exists, read file, otherwise throw runtime error.
			if (fs::exists(p)) {
				stream.open(p);
				std::string line;
				std::stringstream ss;
				if (stream.is_open()) {
					ss << stream.rdbuf();
				}
				stream.close();
				return ss.str();
			}
			else {
				throw std::runtime_error("Unable to open file - " + path);
			}
		}
	};
	
} }
