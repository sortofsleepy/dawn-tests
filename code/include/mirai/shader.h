#pragma once

#include <string>
#include <utils/WGPUHelpers.h>
#include "log.h"

namespace mirai
{

	typedef  std::shared_ptr<class Shader>ShaderRef;
	
	class Shader
	{
	public:


		/**
		 * Format objects for shaders simply store and handle shader source code. It also provides
		 * a simple interface to check what shaders are set and other helpful utilities. 
		 */
		struct Format
		{

			bool hasVertex()
			{
				return _vertex.empty() == true ? false : true;
			}

			bool hasFragment()
			{
				return _fragment.empty() == true ? false : true;
			}

			bool hasCompute()
			{
				return _compute.empty() == true ? false : true;
			}

			std::string getVertexEntry() { return _vertexEntry; }
			std::string getFragmentEntry() { return _fragmentEntry; }

			std::string getVertex() { return _vertex; }
			std::string getFragment() { return _fragment; }
		
			Format& vertex(std::string source,std::string entry="main")
			{
				
				_vertex = source;
				_vertexEntry = entry;
				return *this;
			}
			Format& fragment(std::string source, std::string entry = "main")
			{
				_fragment = source;
				_fragmentEntry = entry;
				return *this;
			}

			Format& compute(std::string source, std::string entry = "main")
			{
				_compute = source;
				_computeEntry = entry;
				return *this;
			}

	
		private:
			std::string _vertex;
			std::string _vertexEntry;
			
			std::string _fragment;
			std::string _fragmentEntry;

			std::string _compute;
			std::string _computeEntry;

		};

	
		Shader(Format fmt);

		static ShaderRef create(Format fmt)
		{
			return ShaderRef(new Shader(fmt));
		}


		//! Returns the source set for the vertex shader 
		std::string getVertexEntry() { return format.getVertexEntry(); }

		//! Returns the source set for the fragment shadeer.
		std::string getFragmentEntry() { return format.getFragmentEntry(); }

		//! Generates shader stages.
		void generateModules(wgpu::Device *device);

		//! deletes shader modules. Do so AFTER you compile pipeline.
		//! If you've generated a compute module, pass in true to release that as well.
		void releaseModules(bool releaseCompute=false);

		//! Returns the vertex stage for shader. 
		WGPUProgrammableStageDescriptor getVertexStage() const
		{

			// warn if generateModules has not been called yet.
			if (!modulesGenerated)
			{
				LOG_W("Note that shader modules may not have been generated");
			}
			return vertexStage;
		}

		//! Returns the fragment stage for the shader. 
		WGPUProgrammableStageDescriptor getFragmentStage() const
		{

			// warn if generateModules has not been called yet.
			if (!modulesGenerated)
			{
				LOG_W("Note that shader modules may not have been generated");
			}
			return fragmentStage;
		}

	
	private:
	
		WGPUProgrammableStageDescriptor vertexStage,fragmentStage,computeStage;
		Format format;
		bool modulesGenerated;
		
	};
}
