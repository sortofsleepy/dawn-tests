#pragma once
#include <utils/WGPUHelpers.h>


namespace mirai { namespace utils {


	//! Produces a WGPUBlendDescriptor object that should be suitable for most situations.
	static WGPUBlendDescriptor defaultBlendDescriptor()
	{
		WGPUBlendDescriptor blend;
		blend.operation = WGPUBlendOperation_Add;
		blend.srcFactor = WGPUBlendFactor_One;
		blend.dstFactor = WGPUBlendFactor_One;

		return blend;
	}
	
} }
